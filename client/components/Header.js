import React, { Component } from "react";
import { graphql } from "react-apollo";
import query from "../queries/CurrentUser";
import { Link } from "react-router";
import mutation from "../mutations/Logout";
class Header extends Component {
  render() {
    return (
      <nav className="nav-wrapper">
        <Link className="brand-logo left" to="/">
          Home
        </Link>
        <ul className="right">{this.renderButtons()}</ul>
      </nav>
    );
  }
  renderButtons() {
    const { user, loading } = this.props.data;

    if (loading) {
      return <div />;
    }
    if (user) {
      return (
        <li>
          <a onClick={this.onLogoutClick.bind(this)}>Logout</a>
        </li>
      );
    }
    return (
      <div>
        <li>
          <Link to="/signup">Signup</Link>
        </li>
        <li>
          <Link to="/login">Login</Link>
        </li>
      </div>
    );
  }

  onLogoutClick() {
    this.props.mutate({
      refetchQueries: [
        {
          query,
        },
      ],
    });
  }
}

export default graphql(mutation)(graphql(query)(Header));
